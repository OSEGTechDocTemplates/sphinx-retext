==========
Evaluation
==========

Why, and what we (probably) choose for the DIN Guideline specification as technical back-end.

Requirements
============

* SCM (git) friendly sources
* open source formats, tools and platforms
* cross-platform
* automatically built and deployed/hosted on push to SCM
* plaint-text readable sources
* pretty, web-hosted HTML output
* well formatted PDF output
* (UNIX philosophy 1st) modularity (the system connects different technologies handling different aspects, instead of using a monolithic solution)
* (UNIX philosophy 2nd) flexibility (parts of the system, or the whole system can be changed)

Options
=======

* `Sphinx <https://sphinx-doc.org>`_
    * `GitHub <https://github.com>`_
    * `GitLab <https://gitlab.com>`_
    * `Gitea <https://gitea.com>`_
    * `ReadTheDocs <https://read-the-docs.io>`_
    * `Markdown <https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>`_
    * `reStructuredText <https://en.wikipedia.org/wiki/ReStructuredText>`_
* Markdown -> HTML - statically converted like on GitHub pages
* Markdown views as HTML - with a web-viewer like when viewing the README.md on a GitHub repo
* MkDocs
* `DocuBricks <https://www.docubricks.com/>`_
* `Wikifactory <https://wikifactory.com/>`_
* `Antora with AsciiDoc <https://antora.org/>`_ (`*.adoc`) sources.
  Antora is brand-new, and focuses on the compilation
  of a single documentation document/site from various git repositories.2
* `GitBook <https://www.gitbook.com/>`_ uses markdown sources on a git repo to compile a PDF/site.
  Seems sub-optimal for our use-case, and is also quite commercial by now.
* `Jekyll <https://jekyllrb.com/>`_ static site generator (written in ruby).
  One writes markdown and/or HTML sources (plus CSS),
  combined with variables/basic code,
  and gets an HTML site.
  Widely used, comfortable, transparent, totally open source, well tested (yeah I like it ;-) ).
  It is rather targeted towards blogs though.
* `Jupyter Notebook/Hub <https://jupyter.org/>`_  is something like Matlab++ as open source,
  focused on python but not limited to it.
  It might make sense to think about integration of it into whatever solution we choose,
  though it probably does not make sense as the main solution,
  neither for the guideline nor the hardware projects documentation.

Choice and rationale
====================

Current choice (as of July 2019)
--------------------------------

Sphinx + reStructuredText + GitLab

This means, our sources area set of reStructuredText files, linking to each other.
They are hosted on GitLab, either on their server, or on our own, using the GitLab CE software.
We use the GitLab built-in build server, and make it run Sphinx.
Sphinx takes the reText flies as input, and generates static HTML output
(and optionally PDF output as well).
This HTML output is then pushed to the GitLab-pages associated with the repository.
The finished documentation is then available under a URL like `https://myUser.gitlab.io/myRepo/`.

This solution fulfills almost all requirements perfectly,
imperfect only regarding flexibility.

.. note:: The first time one publishes to the GitLab-pages,

    it will take some hours (better wait for a whole day)
    until the page is actually online under the gitlab.io URL.

Old choice (as of June 2019)
----------------------------

Sphinx + reStructuredText + GitLab

This means, our sources area set of reStructuredText files, linking to each other.
They are hosted on GitLab, either on their server, or on our own, using the GitLab CE software.
We use the GitLab built-in build server, and make it run Sphinx.
Sphinx takes the reText flies as input, and generates static HTML output
(and optionally PDF output as well).
This HTML output is then pushed to the GitLab-pages associated with the repository.
The finished documentation is then available under a URL like `https://myUser.gitlab.io/myRepo/`.

This solution fulfills almost all requirements perfectly,
imperfect only regarding flexibility.

.. note:: The first time one publishes to the GitLab-pages,

    it will take some hours (better wait for a whole day)
    until the page is actually online under the gitlab.io URL.

Issues with the other solutions
-------------------------------

* GitHub is not open source software, and belongs to MS
* Gitea has no support for build server or pages integration
* ReadTheDocs requires one to give it full read & write access to ones repositories!    (.. and belongs ot MS)
* using pure Markdown as source and "abusing" GitLabs or Giteas direct Markdown rendering,
  or statically converting to HTML, has some technical limitations,
  as it was never meant to be used as a website tool.
  For example, one can not link to an other file in a way that would work for multiple output formats.
* DocuBricks & WikiFactory are meant for Open Hardware projects documentation,
  and are thus not suitable for this project.
  They should be re-considered for Open Hardware projects documentation again, though!
