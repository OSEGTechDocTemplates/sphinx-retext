=====
Setup
=====

1. Prerequisits
===============

* `git <https://git-scm.com/book/en/v2/Getting-Started-Installing-Git>`_
* `python 3.5+ <https://docs.python-guide.org/starting/installation/>`_
* `pip <https://pip.pypa.io/en/stable/installing/>`_
* `sphinx <http://sphinx-doc.org>`_

Debian/Ubuntu Linux install instructions::

	sudo apt-get install git python3-pip
	pip3 install --upgrade sphinx sphinx_rtd_theme

2. Create git repository
========================

.. note:: If you already have a repository, switch to TODO

	TODO second line of the note?

TODO

3. Setup Sphinx project
=======================

Switch to your local git repository (potentially still empty)
that will contain the documentation sources.
Then run the Sphinx project creation assistant::

	sphinx-quickstart

You can use the default values provided (just press *Enter*) for most settings
except maybe these:

* ``Project name``
* ``Author name(s)``
* ``Project release []``
* ``Source file suffix [.rst]: .md``
* ``imgmath: include math, rendered as PNG or SVG images (y/n) [n]: y``
* ``githubpages: create .nojekyll file to publish the document on GitHub pages (y/n) [n]: y``

